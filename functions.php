<?php
// hook wp_head --------------------------------------------------------------------------------
require get_template_directory() . '/functions/head.php';

// wordpress basic options --------------------------------------------------------------------------------
//@codex https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/add_theme_support
function my_setup() {
  add_theme_support( 'post-thumbnails' ); /* アイキャッチ */
  add_theme_support( 'automatic-feed-links' ); /* RSSフィード */
  add_theme_support( 'title-tag' ); /* タイトルタグ自動生成 */
  add_theme_support( 'html5', array( /* HTML5のタグで出力 */
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );
}
add_action( 'after_setup_theme', 'my_setup' );


 // add menu --------------------------------------------------------------------------------
 //@codex https://wpdocs.osdn.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/register_nav_menus
function my_menu_init() {
  register_nav_menus( array(
    'global'  => 'グローバルメニュー',
    'utility' => 'ユーティリティメニュー',
    'drawer'  => 'ドロワーメニュー',
  ) );
}
add_action( 'init', 'my_menu_init' );

 // read script style sheet --------------------------------------------------------------------------------
function my_script_init() {
  wp_enqueue_style( 'style-name', get_template_directory_uri() . '/css/bootstrap.css', array(), '1.0.0', 'all' );
  wp_enqueue_style( 'common_css', get_template_directory_uri() . '/css/common.min.css', array(), '1.0.0', 'all' );
}
add_action( 'wp_enqueue_scripts', 'my_script_init' );

function my_load_widget_scripts() {
    wp_enqueue_script('jquery_js', get_template_directory_uri() . '/js/jquery-3.4.1.min.js', array());
    // Bootstrap_script
    wp_enqueue_script('bootstrap_js', get_template_directory_uri() . '/js/bootstrap.bundle.js', array());
    // common_script
    wp_enqueue_script('common_js', get_template_directory_uri() . '/js/common.js', array());
}

// wp_footerに処理を登録
add_action('wp_footer', 'my_load_widget_scripts');
 ?>
