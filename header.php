<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title('|', true, 'right'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <nav>
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <a href="<?php site_url() ?>"><h1><?php bloginfo( 'name' ); ?></h1></a>
              <p><?php bloginfo( 'description' ); ?></p>
            </div>
            <div class="col-md-6"></div>
          </div>
        </div>
      </nav>
    </header>
